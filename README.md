## Summit View Archive
Developed by Joey Slater

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Common Scripts
* `npm start` : Starts the development server.
* `npm run build` : Bundles the app into static files for production.
* `npm test` : Starts the test runner.

## Screenshots & Animated Demos

##### Demo Shot
![Demo 1](assets/example-1.PNG "Example 1")

##### Mobile Demo
![Mobile 1](assets/mobile-1.PNG "Mobile 1")
![Mobile 1](assets/mobile-2.PNG "Mobile 2")

##### Test Result
Would generally create more tests, ensured it worked with creating the application.
![Demo 1](assets/test-1.PNG "Example 2")

See Assets for Animated GIFS if not running