import {N800, primary} from '@atlaskit/theme/dist/cjs/colors';
import {createMuiTheme} from '@material-ui/core';

/**
 * Creates the theme for an Atlassian product.
 *
 * While the Atlassian documentation seems to use styled-components, I am more familiar and have access to
 * more common ui components to create the project.
 *
 * @type {Theme}
 */
const AtlassianTheme = createMuiTheme({
	palette: {
		primary: {
			main: primary(),
		},
		background: {
			default: '#fff'
		}
	},
	typography: {
		useNextVariants: true,
		// Taken from: https://atlassian.design/guidelines/product/foundations/typography
		h1: {
			characterSpacing: '-0.01em',
			color: N800,
			fontSize: 35,
			fontWeight: 500,
			lineHeight: '40px',
			marginTop: 52
		},
		h2: {
			characterSpacing: '-0.01em',
			color: N800,
			fontSize: 29,
			fontWeight: 500,
			lineHeight: '32px',
			marginTop: 16
		},
		h4: {
			characterSpacing: '-0.008em',
			color: N800,
			fontSize: 20,
			fontWeight: 500,
			lineHeight: '24px',
			marginTop: 36
		},
		fontFamily: [
			'-apple-system',
			'BlinkMacSystemFont',
			'"Segoe UI"',
			'Roboto',
			'Oxygen',
			'Ubuntu',
			'Fira Sans',
			'Droid Sans',
			'"Helvetica Neue"',
			'sans-serif'
		].join(','),
	},
	// Removing all shadows
	shadows: [
		'none', 'none', 'none', 'none', 'none',
		'none', 'none', 'none', 'none', 'none',
		'none', 'none', 'none', 'none', 'none',
		'none', 'none', 'none', 'none', 'none',
		'none', 'none', 'none', 'none', 'none',
	]
});

export default AtlassianTheme;