import AtlassianLogo from '@atlaskit/logo/dist/cjs/AtlassianLogo/Logo';
import {withStyles} from '@material-ui/core';
import Hidden from '@material-ui/core/Hidden/Hidden';
import Tab from '@material-ui/core/Tab/Tab';
import Tabs from '@material-ui/core/Tabs/Tabs';
import Typography from '@material-ui/core/Typography/Typography';
import React, {Component} from 'react';
import SessionApi from '../../services/SessionApi';
import TrackOverview from './TrackOverview/TrackOverview';

let styles = (theme) => ({
	pageTitle: {
		color: '#172B4D',
		display: 'inline-block',
		fontSize: '1.75rem',
		marginLeft: '8px',
	},
	pageSubtitle: {
		color: '#6F6F71',
		fontWeight: 450,
		fontSize: '1.2rem',
		margin: 0
	},
	centered: {
		display: 'flex',
		textAlign: 'center',
		justifyContent: 'center',
		alignItems: 'center',
	},
	tabsRoot: {
		marginTop: 60,
	},
	indicator: {
		backgroundColor: 'white',
		margin: '0 1px',
		transition: 'none'
	},
	tabRoot: {
		minWidth: '0 !important',
		border: `1px solid ${theme.palette.grey['400']}`,
		borderRight: 0,
		backgroundColor: theme.palette.grey['100'],
		'&:first-child': {
			borderTopLeftRadius: 8
		},
		'&:last-child': {
			borderRight: `1px solid ${theme.palette.grey['400']}`,
			borderTopRightRadius: 8
		},
		'&$tabSelected': {
			backgroundColor: theme.palette.common.white,
			borderBottom: 0,
		}
	},
	tabSelected: {}
});

class SummitViewArchive extends Component {
	_tabChangeHandler = (event, selectedTrackIndex) => {
		let trackTitle = this.state.tracks[selectedTrackIndex]['Title'];
		this.props.history.push(`/track/${trackTitle}`);

		this.setState({
			selectedTrackIndex
		});
	};

	constructor(props) {
		super(props);

		this.state = {
			tracks: [],
			selectedTrackIndex: 0,
			selectedTrackTitle: this.props.match.params.track
		};
	}

	componentDidMount() {
		let tracks = SessionApi.getArchiveData();
		let selectedTrackIndex = 0;

		if (this.state.selectedTrackTitle) {
			selectedTrackIndex = tracks.map(track => track['Title']).indexOf(this.state.selectedTrackTitle);
		}

		// Reset the index in case the input is wrong
		if (selectedTrackIndex < 0) {
			selectedTrackIndex = 0;
			// Reset the url when needed
			this.props.history.push(`/track/${tracks[selectedTrackIndex]['Title']}`);
		}

		this.setState({
			tracks: tracks,
			selectedTrackIndex: selectedTrackIndex
		});
	}

	render() {
		const {classes, theme} = this.props;
		const {tracks, selectedTrackIndex} = this.state;

		return (
			<div>
				<Typography variant='h1' className={classes.centered}>
					<AtlassianLogo iconColor={theme.palette.primary.main} label='App Logo : Atlassian'/>
					<span className={classes.pageTitle}>Summit</span>
				</Typography>
				<Typography variant='h2' className={classes.centered}>
					<span className={classes.pageSubtitle}>Video Archive</span>
				</Typography>
				<Hidden xsDown>
					<Tabs value={selectedTrackIndex} onChange={this._tabChangeHandler} scrollable
					      scrollButtons='auto' classes={{root: classes.tabsRoot, indicator: classes.indicator}}
					>
						{this._getTrackTabs()}
					</Tabs>
				</Hidden>
				{
					// Don't add the TrackOverview if no tracks exist
					tracks.length > 0 &&
					<TrackOverview track={tracks[selectedTrackIndex]} key={tracks[selectedTrackIndex]['Title']}/>
				}
			</div>
		);
	}

	_getTrackTabs() {
		let tabs = [];
		for (let track of this.state.tracks) {
			tabs.push(
				<Tab label={track['Title']} key={track['Title']} disableRipple
				     classes={{root: this.props.classes.tabRoot, selected: this.props.classes.tabSelected}}/>
			);
		}
		return tabs;
	}
}

export default withStyles(styles, {withTheme: true})(SummitViewArchive);