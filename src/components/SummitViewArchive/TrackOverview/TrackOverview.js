import Grid from '@material-ui/core/Grid/Grid';
import Hidden from '@material-ui/core/Hidden/Hidden';
import List from '@material-ui/core/List/List';
import ListItem from '@material-ui/core/ListItem/ListItem';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography/Typography';
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import SessionView from './SessionView/SessionView';

const styles = theme => ({
	root: {
		backgroundColor: theme.palette.common.white,
	},
	primaryListItemText: {
		fontWeight: 'bold !important',
		color: theme.palette.primary.main
	},
	listBlock: {
		padding: 16,
		marginLeft: 48
	},
	sessionBlock: {
		padding: 16,
		marginRight: 48
	}
});

/**
 *Ui Component for showing Track Information
 */
class TrackOverview extends Component {

	constructor(props) {
		super(props);

		this.state = {
			selectedSessionIndex: 0
		};
	}

	componentDidMount() {
		this._updateSessionIfUrlParams();
	};

	_updateSessionIfUrlParams() {
		let selectedSessionIndex = 0;
		let sessionId = this.props.match.params.id;

		const {track} = this.props;
		const sessions = track['Sessions'] || [];

		// If there is a session if from the params, else grab the first
		if (sessionId) {
			selectedSessionIndex = sessions.map(session => session['Id']).indexOf(sessionId);
		}

		// If invalid, set the selected to be the first
		if (selectedSessionIndex < 0) {
			selectedSessionIndex = 0;
		}

		this.setState({
			selectedSessionIndex: selectedSessionIndex
		});
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		// Only update state if different
		if (this.props.location.pathname !== prevProps.location.pathname) {
			this._updateSessionIfUrlParams();
		}
	}

	/**
	 * Creating the list item for each of the sesions
	 * @param sessions
	 * @param classes css classes
	 * @returns {Array} the session components
	 * @private
	 */
	_getSessionListItems(sessions, classes) {
		let sessionComponents = [];
		for (let index = 0; index < sessions.length; index++) {
			let session = sessions[index];
			let speakerDisplay = '';
			if (session['Speakers']) {
				let speaker = session['Speakers'][0];
				speakerDisplay = `${speaker['FirstName']} ${speaker['LastName']}, ${speaker['Company']}`;
			}
			sessionComponents.push(
				<ListItem key={session['Id']} button onClick={() => this._updateSession(index)} divider
				          selected={index === this.state.selectedSessionIndex}>
					<ListItemText primary={session['Title']} secondary={speakerDisplay}
					              classes={{primary: classes.primaryListItemText}}/>
				</ListItem>
			);
		}
		return sessionComponents;
	}

	/**
	 * Click handler for the list to update which session to display
	 * @param sessionIndex
	 * @private
	 */
	_updateSession(sessionIndex) {
		this.props.history.push(`/track/${this.props.track['Title']}/session/${this.props.track['Sessions'][sessionIndex]['Id']}`);

		this.setState({
			selectedSessionIndex: sessionIndex
		});
	}

	render() {
		const {classes, track} = this.props;

		const {selectedSessionIndex} = this.state;
		const sessions = track['Sessions'] || [];

		return (
			<div className={classes.root}>
				<Grid container>
					<Hidden xsDown>
						<Grid item sm={6} md={4}>
							<div className={classes.listBlock}>
								<Typography variant='h4'>{track['Title']}</Typography>
								<List component="nav">
									{this._getSessionListItems(sessions, classes)}
								</List>
							</div>
						</Grid>
					</Hidden>
					<Grid item xs={12} sm={6} md={8}>
						<div className={classes.sessionBlock}>
							<SessionView session={sessions[selectedSessionIndex]}/>
						</div>
					</Grid>
				</Grid>
			</div>
		);
	}
}

export default withRouter(withStyles(styles)(TrackOverview));