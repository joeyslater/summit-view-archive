import {withStyles} from '@material-ui/core';
import Typography from '@material-ui/core/Typography/Typography';
import React, {Component} from "react";
import {withRouter} from 'react-router-dom';

const styles = theme => ({
	seeMoreLink: {
		color: theme.palette.primary.main,
		cursor: 'pointer',
		display: 'block',
		fontWeight: 'bold !important',
		margin: '8px 0',
		'&:hover': {
			textDecoration: 'underline',
		}
	},
	speaker: {
		margin: '16px 0',
	}
});

/**
 * Ui Component for showing Session information
 */
class SessionView extends Component {
	render() {
		const {classes, session} = this.props;
		let speaker = session['Speakers'] && session['Speakers'][0];
		let room = session['Room'] && session['Room']['Name'];

		return (
			<div>
				<Typography variant='h2'>
					<strong>
						{session['Title']}
					</strong>
				</Typography>
				<Typography variant='subtitle2'>
					{room || 'N/A'} @ {session['TimeSlot']['StartTime'] || 'N/A'}
				</Typography>
				<Typography variant='subtitle1' className={classes.speaker}>
					<strong>
						{speaker && `${speaker['FirstName']} ${speaker['LastName']}, ${speaker['Company']}`}
					</strong>
				</Typography>
				<div>{session['Description']}</div>
				<span className={classes.seeMoreLink}>See the Q&A from this talk and others here.</span>
				<h2>About the speaker</h2>
				<div>
					{speaker &&
					<span><strong>{speaker['FirstName']} {speaker['LastName']}</strong>, {speaker['Company']}</span>
					}
				</div>
				{speaker && speaker['Bio']}
			</div>
		);
	}
}

export default withRouter(withStyles(styles)(SessionView));