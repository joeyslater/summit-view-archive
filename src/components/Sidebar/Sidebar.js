import {withStyles} from '@material-ui/core';
import List from '@material-ui/core/List/List';
import ListItem from '@material-ui/core/ListItem/ListItem';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader/ListSubheader';
import React, {Component} from "react";
import {withRouter} from 'react-router-dom';
import Link from 'react-router-dom/Link';
import SessionApi from '../../services/SessionApi';

const styles = theme => ({
	list: {
		width: 250,
		backgroundColor: theme.palette.background.paper,
		position: 'relative',
		overflow: 'auto',
	}
});


/**
 * Ui Component for showing Session information
 */
class Sidebar extends Component {

	constructor(props) {
		super(props);

		this.state = {
			tracks: []
		}
	}

	/**
	 * Creates the sidebar menu item
	 * @returns {Array}
	 * @private
	 */
	_getMenuListItems() {
		let menuItems = [];
		for (let track of this.state.tracks) {
			menuItems.push(
				<ListSubheader key={`track-${track['Title']}`}>{track['Title']}</ListSubheader>
			);
			for (let session of track['Sessions']) {
				let link = `/track/${track['Title']}/session/${session['Id']}`;
				menuItems.push(
					<ListItem component={Link} button divider key={session['Id']} to={link}
					          style={{textDecoration: 'none'}}>
						<ListItemText primary={session['Title']}/>
					</ListItem>
				);
			}
		}
		return menuItems;
	}

	componentDidMount() {
		let tracks = SessionApi.getArchiveData();

		this.setState({
			tracks: tracks,
		});
	}

	render() {
		const {classes} = this.props;

		return (
			<List className={classes.list} subheader={<li/>}>
				{this._getMenuListItems()}
			</List>
		);
	}
}

export default withRouter(withStyles(styles)(Sidebar));