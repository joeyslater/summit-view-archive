import AtlassianLogo from '@atlaskit/logo/dist/cjs/AtlassianLogo/Logo';
import {withStyles} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline/CssBaseline';
import Drawer from '@material-ui/core/Drawer/Drawer';
import Hidden from '@material-ui/core/Hidden/Hidden';
import IconButton from '@material-ui/core/IconButton/IconButton';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import React, {Component} from 'react';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import Sidebar from './components/Sidebar/Sidebar';
import SummitViewArchive from './components/SummitViewArchive/SummitViewArchive';
import AtlassianTheme from './theme/AtlassianTheme';


const styles = theme => ({
	menuButton: {
		marginLeft: -12,
		marginRight: 20,
	},
});

/**
 * Entry point for the application (store high level components (router, styles, etc here)
 */
class App extends Component {
	_handleDrawerToggle = () => {
		this.setState(state => ({mobileOpen: !state.mobileOpen}));
	};

	constructor(props) {
		super(props);

		this.state = {
			mobileOpen: false
		}
	}

	render() {
		const {classes} = this.props;
		const {mobileOpen} = this.state;

		return (
			<BrowserRouter>
				<MuiThemeProvider theme={AtlassianTheme}>
					{/*CssBaseline resets the browser styles like normalize.css*/}
					<CssBaseline/>
					<AppBar position='static' color='primary'>
						<Toolbar>
							<Hidden smUp>
								<IconButton color='inherit' className={classes.menuButton} aria-label='Menu'>
									<MenuIcon onClick={this._handleDrawerToggle}/>
								</IconButton>
							</Hidden>
							<AtlassianLogo label='Header Logo : Atlassian'/>
						</Toolbar>
					</AppBar>
					<Drawer open={mobileOpen} onClose={this._handleDrawerToggle}>
						<Sidebar style={{width: 250}}/>
					</Drawer>
					<Switch>
						<Route exact path='/' component={SummitViewArchive}/>
						<Route exact path='/track/:track' component={SummitViewArchive}/>
						<Route exact path='/track/:track/session/:id' component={SummitViewArchive}/>
						<Redirect to='/'/>
					</Switch>
				</MuiThemeProvider>
			</BrowserRouter>
		);
	}
}

export default withStyles(styles)(App);
