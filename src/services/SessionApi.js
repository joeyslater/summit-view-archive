import data from '../dataset/sessions';

/**
 * Create an API that is common and can be called when needed.
 */
class SessionApi {
	/**
	 * Preprocesses the data before visualizing
	 *
	 * For example, it creates a map of {Track, Sessions[]} to better the full data set.
	 *
	 * @param data unprocessed data
	 * @returns {*} processed data
	 * @private
	 */
	static _processData(data) {
		// Get the unique tracks
		let tracks = data['Items'].map(item => JSON.stringify(item['Track']));
		tracks = tracks.filter((track, index) => {
			return tracks.indexOf(track) >= index;
		});
		tracks = tracks.map(track => JSON.parse(track));

		// Add the Sessions array to each track
		for (let track of tracks) {
			track['Sessions'] = [];
		}

		// Get the items based on the track
		data['Items'].reduce((map, item) => {
			let trackTitle = item['Track']['Title'];
			let track = tracks.find(track => track['Title'] === trackTitle);
			track['Sessions'].push(item);
			return map;
		});

		// Sort based on start time
		tracks.forEach(track => {
			track['Sessions'].sort((a, b) => (new Date(a['TimeSlot']['StartTime']) > new Date(b['TimeSlot']['StartTime']))
				? 1 : ((new Date(b['TimeSlot']['StartTime']) > new Date(a['TimeSlot']['StartTime'])) ? -1 : 0))
		});
		return tracks;
	}

	/**
	 * Returns the archive data ready to be displayed
	 *
	 * @returns {*} the sessions data preprocessed and from a cache
	 */
	static getArchiveData() {
		if (!this._cachedData) {
			// Currently pulling from the json file, but is able to hook to fetch / rest API
			this._cachedData = this._processData(data);
		}
		return this._cachedData;
	}
}

export default SessionApi;